import { Component, OnInit } from '@angular/core';
import {Hero} from "../../interfaces/hero.interface";
import {HEROES} from "../../mocks/heroes.mock";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  hero: Hero = {
    id: 1,
    name: "Superman"
  };

  heroes = HEROES;

  constructor() { }

  ngOnInit(): void {
  }

}
